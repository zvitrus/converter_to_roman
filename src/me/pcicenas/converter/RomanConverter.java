package me.pcicenas.converter;

/**
 * A class responsible for converting a Roman number to a decimal number.
 *
 * @author pcicenas
 */
public class RomanConverter {
    private static String romanNumber;
    private static int decimalNumber;

    public RomanConverter(String numberToConvert) {
        this.romanNumber = numberToConvert;
    }

    /**
     * Converts an input of a Roman value to a decimal value.
     *
     * @author pcicenas
     */
    public void convertToDecimal(String numberToConvert) {
        romanNumber = romanNumber.toUpperCase();

        int lengthOfRomanNumber = romanNumber.length();
        int num = 0;
        int previousNum = 0;
        for (int i = lengthOfRomanNumber - 1; i >= 0; i--) {
            char character = romanNumber.charAt(i);
            character = Character.toUpperCase(character);
            switch (character) {
                case 'I':
                    previousNum = num;
                    num = 1;
                    break;
                case 'V':
                    previousNum = num;
                    num = 5;
                    break;
                case 'X':
                    previousNum = num;
                    num = 10;
                    break;
                case 'L':
                    previousNum = num;
                    num = 50;
                    break;
                case 'C':
                    previousNum = num;
                    num = 100;
                    break;
                case 'D':
                    previousNum = num;
                    num = 500;
                    break;
                case 'M':
                    previousNum = num;
                    num = 1000;
                    break;
            }
            if (num < previousNum) {
                decimalNumber = decimalNumber - num;
            } else
                decimalNumber = decimalNumber + num;
        }
    }

    /**
     * A prints a value.
     *
     * @author pcicenas
     */
    public static void printRoman() {
        System.out.println("The equivalent of the Roman numeral " + romanNumber + " is " + decimalNumber);
    }
}

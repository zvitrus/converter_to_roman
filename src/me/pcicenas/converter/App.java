package me.pcicenas.converter;

import java.util.Scanner;

public class App {

    public static void main(String args[]) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a Roman number: ");

        String numberToConvert = scan.nextLine();

        RomanConverter romanConverter = new RomanConverter(numberToConvert);
        romanConverter.convertToDecimal(numberToConvert);
        romanConverter.printRoman();
    }
}
